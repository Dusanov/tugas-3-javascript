//soal nomer satu ----------------------------------------------------------

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var keempat = kataKeempat.toUpperCase();
var kedua = kataKedua.replace("senang", "Senang");

console.log(kataPertama.concat(kedua, kataKetiga, keempat));

//soal nomer dua ----------------------------------------------------------

var kata_Pertama = "1";
var kata_Kedua = "2";
var kata_Ketiga = "4";
var kata_Keempat = "5";

var satu = parseInt(kata_Pertama);
var dua = parseInt(kata_Kedua);
var tiga = parseInt(kata_Ketiga);
var empat = parseInt(kata_Keempat);

console.log(satu + dua + tiga + empat);

//soal nomer 3 ----------------------------------------------------------

var kalimat = 'wah javascript itu keren sekali'; 

var katapertama = kalimat.substring(0, 3); 
var katakedua = kalimat.substring(4, 14); 
var kataketiga = kalimat.substring(15, 18); 
var katakeempat = kalimat.substring(19, 24); 
var katakelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + katapertama); 
console.log('Kata Kedua: ' + katakedua); 
console.log('Kata Ketiga: ' + kataketiga); 
console.log('Kata Keempat: ' + katakeempat); 
console.log('Kata Kelima: ' + katakelima);

// soal nomer 4 ----------------------------------------------------------

var nilai = 65;

if (nilai >= 75) {
	console.log("A")
}else if(nilai >= 70 && nilai <= 80) {
	console.log("B")
}else if(nilai >= 60 && nilai <= 70) {
	console.log("C")
}else if(nilai >= 50 && nilai <= 60) {
	console.log("D")
}else if(nilai <= 50) {
	console.log("E")
}

//soal nomer 5 ----------------------------------------------------------

var tanggal = 21;
var bulan = 11;
var tahun = 1988;

//alternatif 1

// switch(bulan) {
// 	case 1:   	{ console.log(tanggal + ' januari ' + tahun); break; }
// 	case 2:   	{ console.log(tanggal + ' februari ' + tahun); break; }
// 	case 3:   	{ console.log(tanggal + ' maret ' + tahun); break; }
// 	case 4:   	{ console.log(tanggal + ' april ' + tahun); break; }
// 	case 5:   	{ console.log(tanggal + ' mei ' + tahun); break; }
// 	case 6:   	{ console.log(tanggal + ' juni ' + tahun); break; }
// 	case 7:   	{ console.log(tanggal + ' juli ' + tahun); break; }
// 	case 8:   	{ console.log(tanggal + ' agustus ' + tahun); break; }
// 	case 9:   	{ console.log(tanggal + ' septemner ' + tahun); break; }
// 	case 10:   	{ console.log(tanggal + ' oktober ' + tahun); break; }
// 	case 11:   	{ console.log(tanggal + ' november ' + tahun); break; }
// 	case 12:    { console.log(tanggal + ' desember ' + tahun); break;}
// }

//alternatif 2

switch(bulan) {
	case 1:   	{ bulan = 'januari'; break; }
	case 2:   	{ bulan = 'februari'; break; }
	case 3:   	{ bulan = 'maret'; break; }
	case 4:   	{ bulan = 'april'; break; }
	case 5:   	{ bulan = 'mei'; break; }
	case 6:   	{ bulan = 'juni'; break; }
	case 7:   	{ bulan = 'juli'; break; }
	case 8:   	{ bulan = 'agustus'; break; }
	case 9:   	{ bulan = 'september'; break; }
	case 10:   	{ bulan = 'oktober'; break; }
	case 11:   	{ bulan = 'november'; break; }
	case 12:    { bulan = 'desember'; break;}
}

console.log(tanggal + ' ' + bulan + ' ' + tahun)
